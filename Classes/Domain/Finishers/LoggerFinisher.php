<?php
declare(strict_types = 1);
namespace HIVE\HiveOvrForm\Domain\Finishers;

use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Form\Mvc\Property\TypeConverter\PseudoFileReference;

class LoggerFinisher extends \Pagemachine\Formlog\Domain\Form\Finishers\LoggerFinisher
{

    /**
     * Get normalized form values
     * extend 'file' by path, url
     */
    protected function getFormValues(): array
    {
        $normalizedFormValues = [];
        $formDefinition = $this->finisherContext->getFormRuntime()->getFormDefinition();

        foreach ($this->finisherContext->getFormValues() as $identifier => $formValue) {
            if (is_object($formValue)) {

                if ($formValue instanceof ExtbaseFileReference) {
                    $formValue = $formValue->getOriginalResource();
                }

                //teufels-bh: add instanceof PseudoFileReference
                if ($formValue instanceof PseudoFileReference) {

                    $formValue = $formValue->getOriginalResource();

                    $normalizedFormValues[$identifier] = [
                        'file' => [
                            'name' => $formValue->getName(),
                            'path' => $formValue->getIdentifier(),
                            'url' => $formValue->getPublicUrl(),
                        ],
                    ];
                    continue;
                }

                if ($formValue instanceof CoreFileReference) {
                    $normalizedFormValues[$identifier] = [
                        'file' => [
                            'name' => $formValue->getName(),
                            'path' => $formValue->getIdentifier(),
                            'url' => $formValue->getPublicUrl(),
                        ],
                    ];
                    continue;
                }

                $element = $formDefinition->getElementByIdentifier($identifier);

                if ($element instanceof StringableFormElementInterface) {
                    $normalizedFormValues[$identifier] = $element->valueToString($formValue);
                    continue;
                }
            /**
             * 1.0.1 :: add Multiple-File Upload Support (provided by form_extended)
             */
            } else if (is_array($formValue))  {

                $formValues = $formValue;
                $parent_identifier = $identifier;

                foreach ($formValues as $identifier => $formValue) {

                    $identifier = $parent_identifier . "_" . $identifier;

                    if ($formValue instanceof ExtbaseFileReference) {
                        $formValue = $formValue->getOriginalResource();
                    }

                    //teufels-bh: add instanceof PseudoFileReference
                    if ($formValue instanceof PseudoFileReference) {

                        $formValue = $formValue->getOriginalResource();

                        $normalizedFormValues[$identifier] = [
                            'file' => [
                                'name' => $formValue->getName(),
                                'path' => $formValue->getIdentifier(),
                                'url' => $formValue->getPublicUrl(),
                            ],
                        ];
                        continue;
                    }

                    if ($formValue instanceof CoreFileReference) {
                        $normalizedFormValues[$identifier] = [
                            'file' => [
                                'name' => $formValue->getName(),
                                'path' => $formValue->getIdentifier(),
                                'url' => $formValue->getPublicUrl(),
                            ],
                        ];
                        continue;
                    }

                    $element = $formDefinition->getElementByIdentifier($identifier);

                    if ($element instanceof StringableFormElementInterface) {
                        $normalizedFormValues[$identifier] = $element->valueToString($formValue);
                        continue;
                    }
                }

            } else {
                $normalizedFormValues[$identifier] = $formValue;
            }
        }

        return $normalizedFormValues;
    }
}