![VENDOR](https://img.shields.io/badge/vendor-HIVE-%219A83.svg)
![KEY](https://img.shields.io/badge/key-hive__ovr__form-blue.svg)
![version](https://img.shields.io/badge/version-1.2.*-yellow.svg?style=flat-square)

HIVE Override form
==========
HIVE Override/Extend form & Collection from based Extensions

#### This version supports TYPO3
![CUSTOMER](https://img.shields.io/badge/11_LTS-%23A6C694.svg?style=flat-square)

#### Composer support
`composer req beewilly/hive_ovr_form`

## Requirements
- `"typo3/cms-form": "^11.5"`
- `"pagemachine/typo3-formlog": "^2.2"`
- `"wapplersystems/form_extended": "^11.0"`
- `"kitzberger/form-mailtext" : "^2.1"`

## Notice
- Overrides "EmailFinisher" & "LoggerFinisher" for Multiple-File Upload Support (provided by form_extended)
- Overrides "EmailFinisher" to add additional receiver from hive_career > JobOffer to BCC for Finisher MailToReceiver

## Documentation
- https://github.com/WapplerSystems/form_extended

## Changelog
### [1.2.5] - 2023-06-26
- update version of requirements
### [1.2.4] - 2023-06-26
- remove Fix Typo3 core issue Bug #99870 from 1.2.1 not necessary anymore
### [1.2.3] - 2023-06-26
- Override EmailFinisher of form_mailtext & form_extended (fixes error message=null on form_mailtext)
### [1.2.1] - 2023-02-09
- Fix Typo3 core issue Bug #99870 (Undefined array key "templateName" in FormEditorController.php line 548) -> should be fixed with Typo3 11.5.24 -> then remove override
### [1.1.0] - 2023-02-09
- require wapplersystems/form_extended & overrides + fixes for form_extended
### [1.0.1] - 2023-02-07
- Overrides "EmailFinisher" & "LoggerFinisher" for Multiple-File Upload Support (provided by form_extended)
### [1.0.0] - 2023-02-07
- Overrides "EmailFinisher" to add additional receiver from hive_career > JobOffer to BCC for Finisher MailToReceiver