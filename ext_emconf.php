<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'HIVE>Override form',
    'description' => 'HIVE Override form',
    'category' => 'plugin',
    'author' => 'teufels GmbH',
    'author_email' => 'digital@teufels.com',
    'state' => 'beta',
    'uploadfolder' => 1,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.2.6',
    'constraints' => [
        'depends' => [
            'form' => '11.5.0-11.9.99',
            'typo3-formlog' => '>=2.2.1',
            'form_extended' => '11.0.7',
            'form-mailtext' => '>=2.1.4',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
