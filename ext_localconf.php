<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(function() {

    /**
     * 1.0.0 :: Override LoggerFinisher with slightly different one (that extend 'file' by path,url)
     * 1.0.1 :: Override LoggerFinisher add Multiple-File Upload Support (provided by form_extended)
     */
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\Pagemachine\Formlog\Domain\Form\Finishers\LoggerFinisher::class] = [
        'className' => \HIVE\HiveOvrForm\Domain\Finishers\LoggerFinisher::class
    ];

    /**
     * 1.0.0 :: Override EmailFinisher add additional receiver from hive_career > JobOffer to BCC for Finisher MailToReceiver
     * 1.0.1 :: Override EmailFinisher add Multiple-File Upload Support (provided by form_extended)
     * 1.2.3 :: Override EmailFinisher of form_mailtext & form_extended (fixes error message=null on form_mailtext)
     */
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\Kitzberger\FormMailtext\Domain\Finishers\EmailFinisher::class] = [
        'className' => \HIVE\HiveOvrForm\Domain\Finishers\EmailFinisher::class
    ];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\WapplerSystems\FormExtended\Domain\Finishers\EmailFinisher::class] = [
        'className' => \HIVE\HiveOvrForm\Domain\Finishers\EmailFinisher::class
    ];

    /**
     * 1.1.0 :: Fix error Exception while property mapping at property path "": Property "0" was not found in target object of type "TYPO3\CMS\Extbase\Domain\Model\FileReference".
     * on back with multiple files & forms with steps
     */
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Form\ViewHelpers\Form\UploadedResourceViewHelper::class] = [
        'className' => \HIVE\HiveOvrForm\ViewHelpers\UploadedResourcesViewHelper::class
    ];

});